//
//  ViewController.swift
//  Connect
//
//  Created by Thiago Oliveira on 28/06/2018.
//  Copyright © 2018 Upgrade. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var devView: UIView!
    @IBOutlet weak var darkFillView: UIViewX!
    @IBOutlet weak var toggleButton: UIButton!
    @IBOutlet weak var circImgView: UIImageView!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }

    @IBAction func toggleButton(_ sender: UIButton) {
        
        if darkFillView.transform == .identity {
            UIView.animate(withDuration: 1, animations: {
                self.darkFillView.transform = CGAffineTransform(scaleX: 33, y: 33)
                self.devView.transform = CGAffineTransform(translationX: 0, y: -414)
                self.circImgView.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.circImgView.transform = CGAffineTransform(translationX: -65, y: -195)
                self.toggleButton.transform = CGAffineTransform(rotationAngle: self.radians(180))
            }) { (true) in
            }
        } else {
            UIView.animate(withDuration: 1, animations: {
                self.darkFillView.transform = .identity
                self.devView.transform = .identity
                self.circImgView.transform = .identity
                self.toggleButton.transform = .identity
                
            })
        }
    }
    
    
    
    
    func radians (_ degrees: Double) -> CGFloat {
        return CGFloat(degrees * .pi / degrees)
    }

}

